import math

def is_square(n):    
    try:
        root = int(math.sqrt(n))
        return root * root == n
    except:
        return False     